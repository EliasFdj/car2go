<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211206014022 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE car ADD annee VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE car ADD short_desc TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE car ADD boite VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE car ADD portes INT DEFAULT NULL');
        $this->addSql('ALTER TABLE car ADD carburant VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE car ADD marque VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE car ADD prix NUMERIC(10, 2) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE car DROP annee');
        $this->addSql('ALTER TABLE car DROP short_desc');
        $this->addSql('ALTER TABLE car DROP boite');
        $this->addSql('ALTER TABLE car DROP portes');
        $this->addSql('ALTER TABLE car DROP carburant');
        $this->addSql('ALTER TABLE car DROP marque');
        $this->addSql('ALTER TABLE car DROP prix');
    }
}
